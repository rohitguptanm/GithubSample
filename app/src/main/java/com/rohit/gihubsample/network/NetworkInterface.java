package com.rohit.gihubsample.network;

import com.rohit.gihubsample.models.ContributorPojo;
import com.rohit.gihubsample.models.ItemsItem;
import com.rohit.gihubsample.models.SearchRepoPojo;


import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface NetworkInterface {

 @GET("search/repositories")
 Observable<SearchRepoPojo> getSearchRepository(@QueryMap Map<String,String> map);

// rohitguptanm/AndroidDemoProjects

 @GET("repos/{login}/{name}/contributors")
 Observable<List<ContributorPojo>> getContributors(@Path("login") String login,@Path("name") String name);

 //rohitguptanm
 @GET("users/{login}/repos")
 Observable<List<ItemsItem>> getRepoList(@Path("login") String login);

}
