package com.rohit.gihubsample.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rohit.gihubsample.R;
import com.rohit.gihubsample.models.ContributorPojo;
import com.rohit.gihubsample.ui.contributorRepo.ContributorRepoActivity;
import com.rohit.gihubsample.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by anujgupta on 26/12/17.
 */

public class ContributorAdapter extends RecyclerView.Adapter<ContributorAdapter.ContributorHolder> {

    private List<ContributorPojo> mContributorPojoList;
    private Context context;


    public ContributorAdapter(Context context, List<ContributorPojo> mContributorPojoList) {
        this.context = context;
        this.mContributorPojoList = mContributorPojoList;
    }

    @Override
    public ContributorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_contributor, parent, false);
        ContributorHolder ch = new ContributorHolder(v);
        return ch;
    }

    @Override
    public void onBindViewHolder(ContributorHolder holder, final int position) {

        holder.tvName.setText(mContributorPojoList.get(position).getLogin());
        Glide.with(context).load(mContributorPojoList.get(position).getAvatarUrl()).into(holder.ivAvtar);

        holder.mCvRepoRaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContributorRepoActivity.class);
                intent.putExtra(AppConstants.LOGIN, mContributorPojoList.get(position).getLogin());
                intent.putExtra(AppConstants.AVTAR_IMG, mContributorPojoList.get(position).getAvatarUrl());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContributorPojoList.size();
    }

     class ContributorHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_avtar)
        ImageView ivAvtar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.cv_repo_raw)
        CardView mCvRepoRaw;

         ContributorHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
