package com.rohit.gihubsample.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rohit.gihubsample.R;
import com.rohit.gihubsample.models.ItemsItem;
import com.rohit.gihubsample.ui.repoDetail.RepoDetailsActivity;
import com.rohit.gihubsample.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by anujgupta on 26/12/17.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.MoviesHolder> {

    List<ItemsItem> repoList;
    Context context;


    public RepoAdapter(Context context,List<ItemsItem> repoList) {
        this.context = context;
        this.repoList = repoList;
    }

    @Override
    public MoviesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_repo, parent, false);
        MoviesHolder mh = new MoviesHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(MoviesHolder holder, final int position) {

        holder.tvName.setText(repoList.get(position).getName());
        holder.tvFullName.setText(repoList.get(position).getFullName());
        holder.tvWatcherCount.setText(repoList.get(position).getWatchersCount()+"");
//        holder.tvCommitCount.setText(repoList.get(position).getReleaseDate());
        Glide.with(context).load(repoList.get(position).getOwner().getAvatarUrl()).into(holder.ivAvtar);

        holder.mCvRepoRaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RepoDetailsActivity.class);
                intent.putExtra(AppConstants.NAME,repoList.get(position).getName());
                intent.putExtra(AppConstants.LOGIN,repoList.get(position).getOwner().getLogin());
                intent.putExtra(AppConstants.AVTAR_IMG,repoList.get(position).getOwner().getAvatarUrl());
                intent.putExtra(AppConstants.PROJECT_DESC,repoList.get(position).getDescription());
                intent.putExtra(AppConstants.PROJECT_LINK,repoList.get(position).getSvnUrl());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public class MoviesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_avtar)
        ImageView ivAvtar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_full_name)
        TextView tvFullName;
        @BindView(R.id.tv_watcher_count)
        TextView tvWatcherCount;
        @BindView(R.id.tv_commit_count)
        TextView tvCommitCount;
        @BindView(R.id.cv_repo_raw)
        CardView mCvRepoRaw;

        public MoviesHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
