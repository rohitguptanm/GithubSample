package com.rohit.gihubsample.ui.repoDetail;

import com.rohit.gihubsample.models.ContributorPojo;
import com.rohit.gihubsample.models.SearchRepoPojo;

import java.util.List;

public interface RepoDetailsViewInterface {

    void showProgressBar();
    void hideProgressBar();
    void contributorList(List<ContributorPojo> contributorList);
    void showMsg(String s);

}
