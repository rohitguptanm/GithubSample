package com.rohit.gihubsample.ui.repoDetail;

import android.util.Log;

import com.rohit.gihubsample.models.ContributorPojo;
import com.rohit.gihubsample.models.SearchRepoPojo;
import com.rohit.gihubsample.network.NetworkClient;
import com.rohit.gihubsample.network.NetworkInterface;
import com.rohit.gihubsample.ui.home.HomePresenterInterface;
import com.rohit.gihubsample.ui.home.HomeViewInterface;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class RepoDetailsPresenter implements RepoDetailsPresenterInterface {

    RepoDetailsViewInterface mRepoDetailsViewInterface;

    private String TAG = "HomePresenter";

    public RepoDetailsPresenter(RepoDetailsViewInterface rvi) {
        mRepoDetailsViewInterface = rvi;
    }

    public Observable<List<ContributorPojo>> getObservable(String login,String name){
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getContributors(login,name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<List<ContributorPojo>> getObserver(){
        return new DisposableObserver<List<ContributorPojo>>() {

            @Override
            public void onNext(List<ContributorPojo> contributorPojos) {
                Log.d(TAG, "onNext: "+contributorPojos.size());
                mRepoDetailsViewInterface.hideProgressBar();
                mRepoDetailsViewInterface.contributorList(contributorPojos);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: "+e.getMessage());
                mRepoDetailsViewInterface.hideProgressBar();
                mRepoDetailsViewInterface.showMsg(e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        };
    }

    @Override
    public void getContributor(String login,String name) {
        mRepoDetailsViewInterface.showProgressBar();
        getObservable(login,name).subscribeWith(getObserver());
    }
}
