package com.rohit.gihubsample.ui.home;

import android.util.Log;

import com.rohit.gihubsample.models.SearchRepoPojo;
import com.rohit.gihubsample.network.NetworkClient;
import com.rohit.gihubsample.network.NetworkInterface;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class HomePresenter implements HomePresenterInterface {

    HomeViewInterface mHomeViewInterface;

    private String TAG = "HomePresenter";

    public HomePresenter(HomeViewInterface hvi) {
        mHomeViewInterface = hvi;
    }

    @Override
    public void getRepo(String query,String order,boolean isProgressbar) {
        if(isProgressbar)
        mHomeViewInterface.showProgressBar();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("q", query);
        if(order.isEmpty()) {
            hashMap.put("order", "desc");
        }else{
            hashMap.put("order", order);
        }
        getObservable(hashMap).subscribeWith(getObserver());
    }

    public Observable<SearchRepoPojo> getObservable(HashMap<String, String> hashMap) {
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getSearchRepository(hashMap)
                .subscribeOn(Schedulers.io())
                .debounce(400, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<SearchRepoPojo> getObserver() {
        return new DisposableObserver<SearchRepoPojo>() {
            @Override
            public void onNext(SearchRepoPojo searchRepoPojo) {
                Log.d(TAG, "onNext: " + searchRepoPojo.getTotalCount());
                mHomeViewInterface.hideProgressBar();
                mHomeViewInterface.displayRepo(searchRepoPojo);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: " + e.getMessage());
                mHomeViewInterface.hideProgressBar();
                try {
                    HttpException httpException = (HttpException) e;
                    int code = httpException.response().code();
                    if (code == 403) {
                        mHomeViewInterface.displayError("Api limit exceed");
                        return;
                    }
                }catch (Exception exception){
                    exception.printStackTrace();
                }
                mHomeViewInterface.displayError(e.getMessage());

            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        };
    }

}
