package com.rohit.gihubsample.ui.contributorRepo;

import android.util.Log;

import com.rohit.gihubsample.models.ItemsItem;
import com.rohit.gihubsample.network.NetworkClient;
import com.rohit.gihubsample.network.NetworkInterface;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ContributorRepoPresenter implements ContributorRepoPresenterInterface {

    ContributorRepoViewInterface mContributorRepoViewInterface;

    private String TAG = "HomePresenter";

    public ContributorRepoPresenter(ContributorRepoViewInterface cvi) {
        mContributorRepoViewInterface = cvi;
    }



    public Observable<List<ItemsItem>> getObservable(String login){
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getRepoList(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<List<ItemsItem>> getObserver(){
        return new DisposableObserver<List<ItemsItem>>() {

            @Override
            public void onNext(List<ItemsItem> contributorRepoPojoList) {
                Log.d(TAG, "onNext: "+contributorRepoPojoList.size());
                mContributorRepoViewInterface.hideProgressBar();
                mContributorRepoViewInterface.displayRepo(contributorRepoPojoList);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: "+e.getMessage());
                mContributorRepoViewInterface.hideProgressBar();
                mContributorRepoViewInterface.displayError(e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        };
    }

    @Override
    public void getAllRepo(String login) {
        mContributorRepoViewInterface.showProgressBar();
        getObservable(login).subscribeWith(getObserver());
    }
}
