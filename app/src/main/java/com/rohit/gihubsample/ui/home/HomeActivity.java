package com.rohit.gihubsample.ui.home;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.rohit.gihubsample.R;
import com.rohit.gihubsample.adapters.RepoAdapter;
import com.rohit.gihubsample.models.ItemsItem;
import com.rohit.gihubsample.models.SearchRepoPojo;
import com.rohit.gihubsample.utils.Utility;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeViewInterface {

    @BindView(R.id.rvRepo)
    RecyclerView mRvRepo;
    @BindView(R.id.progressbar)
    DilatingDotsProgressBar mDilatingDotsProgressBar;

    HomePresenter mHomePresenter;
    RepoAdapter repoAdapter;
    LinearLayoutManager mLinearLayoutManager;
    List<ItemsItem> repoList = new ArrayList<>();
    public final String TAG = HomeActivity.class.getSimpleName();


    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.switchModifier)
    Switch mSwitchModifier;
    @BindView(R.id.switchArchieved)
    Switch mSwitchArchieved;

    @BindView(R.id.iv_filter)
    ImageView mIvFilter;
    @BindView(R.id.et_start_date)
    EditText etStartDate;
    @BindView(R.id.et_end_date)
    EditText etEndDate;

    Calendar myCalendar;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    String searchValue = "tetris+is:public";
    @BindView(R.id.btn_asc)
    AppCompatButton btnAsc;
    @BindView(R.id.btn_desc)
    AppCompatButton btnDesc;

    String order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        //
        order = getString(R.string.desc);
        btnDesc.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);

        mHomePresenter = new HomePresenter(this);

        mHomePresenter.getRepo(searchValue, "", true);

        repoAdapter = new RepoAdapter(this, repoList);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRvRepo.setLayoutManager(mLinearLayoutManager);
        mRvRepo.setAdapter(repoAdapter);

        myCalendar = Calendar.getInstance();

        mRvRepo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 5 || dy < -5)
                    Utility.hideKeyboard(HomeActivity.this);
            }
        });

        setSupportActionBar(mToolbar);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        mSwitchModifier.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mSwitchModifier.setText(getResources().getString(R.string.private_status));
                } else {
                    mSwitchModifier.setText(getResources().getString(R.string.public_status));
                }
            }
        });

        mIvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(HomeActivity.this, mOnDateSetListenerStart, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(HomeActivity.this, mOnDateSetListenerEndDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    DatePickerDialog.OnDateSetListener mOnDateSetListenerStart = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            String monthValue = String.valueOf(month);
            String dayValue = String.valueOf(dayOfMonth);
            if (monthValue.length() == 1) {
                month++;
                monthValue = "0" + month;
            }

            if (dayValue.length() == 1) {
                dayValue = "0" + dayOfMonth;
            }
            etStartDate.setText(year + "-" + monthValue + "-" + dayValue);
        }
    };

    DatePickerDialog.OnDateSetListener mOnDateSetListenerEndDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            String monthValue = String.valueOf(month);
            String dayValue = String.valueOf(dayOfMonth);
            if (monthValue.length() == 1) {
                month++;
                monthValue = "0" + month;
            }

            if (dayValue.length() == 1) {
                dayValue = "0" + dayOfMonth;
            }
            etEndDate.setText(year + "-" + monthValue + "-" + dayValue);

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Log.d(TAG, "onQueryTextSubmit:1 " + query);
                if (query.length() > 0) {
                    searchValue = query;
                    mHomePresenter.getRepo(query, "", false);
                }
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                item.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String sText) {
                Log.d(TAG, "onQueryTextSubmit:2 " + sText);
                if (sText.length() > 0) {
                    searchValue = sText;
                    mHomePresenter.getRepo(sText, "", false);
                }
                return false;
            }
        });

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void displayRepo(SearchRepoPojo searchRepoPojo) {
        if (searchRepoPojo != null && searchRepoPojo.getItems().size() > 0) {
            Log.d(TAG, searchRepoPojo.getItems().get(0).getName());
            repoList.clear();
            repoList.addAll(searchRepoPojo.getItems());
            repoAdapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "nothing found: ");
        }
    }

    @Override
    public void displayError(String s) {
        showToast(s);
    }


    @Override
    public void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        mDilatingDotsProgressBar.showNow();
    }

    @Override
    public void hideProgressBar() {
        mDilatingDotsProgressBar.hideNow();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {

        StringBuilder stringBuilder = new StringBuilder();

        if (mSwitchModifier.isChecked()) {
            stringBuilder.append("+is:private");
        } else {
            stringBuilder.append("+is:public");
        }

        if (mSwitchArchieved.isChecked()) {
            stringBuilder.append("+archived:true");
        } else {
            stringBuilder.append("+archived:false");
        }

        String startDate = etStartDate.getText().toString().trim();
        String endDate = etEndDate.getText().toString().trim();

        if (!startDate.isEmpty()&& !endDate.isEmpty()) {
            stringBuilder.append(" created:"+startDate+".."+endDate);
        } else if (!startDate.isEmpty()) {
            stringBuilder.append(" created:>="+startDate);  // cats created:>=2016-04-29
        } else if (!endDate.isEmpty()) {
            stringBuilder.append(" created:<="+endDate);
        }

        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mHomePresenter.getRepo(searchValue+stringBuilder.toString(), order, true);
    }

    @OnClick({R.id.btn_asc, R.id.btn_desc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_asc:
                order = getString(R.string.asc);
                btnAsc.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
                btnDesc.getBackground().clearColorFilter();
                break;
            case R.id.btn_desc:
                order = getString(R.string.desc);
                btnDesc.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
                btnAsc.getBackground().clearColorFilter();
                break;
        }
    }
}
