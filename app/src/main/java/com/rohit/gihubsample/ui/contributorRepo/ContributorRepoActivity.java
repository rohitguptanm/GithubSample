package com.rohit.gihubsample.ui.contributorRepo;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rohit.gihubsample.R;
import com.rohit.gihubsample.adapters.RepoAdapter;
import com.rohit.gihubsample.models.ItemsItem;
import com.rohit.gihubsample.utils.AppConstants;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContributorRepoActivity extends AppCompatActivity implements ContributorRepoViewInterface, AppBarLayout.OnOffsetChangedListener {


    ContributorRepoPresenter mContributorRepoPresenter;
    RepoAdapter repoAdapter;
    List<ItemsItem> repoList = new ArrayList<>();
    public final String TAG = ContributorRepoActivity.class.getSimpleName();

    String login = "";
    String avtar_img;

    //constants for view transformation
    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    @BindView(R.id.main_linearlayout_title)
    LinearLayout mTitleContainer;
    @BindView(R.id.main_framelayout_title)
    FrameLayout mainFramelayoutTitle;
    @BindView(R.id.main_appbar)
    AppBarLayout mMainAppbar;
    @BindView(R.id.recyclerview_coordinator_behavior)
    RecyclerView mRvRepo;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.iv_avtar)
    AppCompatImageView mAppCompatImageView;

    @BindView(R.id.progressbar)
    DilatingDotsProgressBar mDilatingDotsProgressBar;

    //boolean to keep track of changes during the appbar transformation.
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_repo);
        ButterKnife.bind(this);

        login = getIntent().getStringExtra(AppConstants.LOGIN);
        avtar_img = getIntent().getStringExtra(AppConstants.AVTAR_IMG);

        mTvTitle.setText(login);

//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Glide.with(this).load(avtar_img).into(mAppCompatImageView);

        mContributorRepoPresenter = new ContributorRepoPresenter(this);
        mContributorRepoPresenter.getAllRepo(login);

        repoAdapter = new RepoAdapter(this, repoList);
        mRvRepo.setLayoutManager(new LinearLayoutManager(this));
        mRvRepo.setAdapter(repoAdapter);


         /*
        set up the offset change listener for the appbar. it will be used for the alpha change animation
        for the title in toolbar, when the appbar transformation is going on.
         */
        mMainAppbar.addOnOffsetChangedListener(this);

        /*
        start the initial alpha animation. it will hide the title completely.
         */
        startAlphaAnimation(mTvTitle, 0, View.INVISIBLE);
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    /**
     * method to handle the alpha change animation for the toolbar title.
     *
     * @param percentage percentage for the alpha of the title
     */
    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(mTvTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(mTvTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public void showToast(String s) {

    }

    @Override
    public void showProgressBar() {
        mDilatingDotsProgressBar.showNow();
    }

    @Override
    public void hideProgressBar() {
        mDilatingDotsProgressBar.hideNow();
    }

    @Override
    public void displayRepo(List<ItemsItem> contributorPojoList) {
        if (contributorPojoList != null && contributorPojoList.size() > 0) {
            Log.d(TAG, contributorPojoList.size() + "");
            repoList.addAll(contributorPojoList);
            repoAdapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "displayRepo: no Repo");
        }
    }

    @Override
    public void displayError(String s) {

    }


}
