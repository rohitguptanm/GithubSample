package com.rohit.gihubsample.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.ConsoleMessage;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.rohit.gihubsample.R;
import com.rohit.gihubsample.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView mWebview;

    String project_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        project_url = getIntent().getStringExtra(AppConstants.PROJECT_LINK);

        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebview.loadUrl(project_url);
    }
}
