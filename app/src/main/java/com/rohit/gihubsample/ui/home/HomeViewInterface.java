package com.rohit.gihubsample.ui.home;

import com.rohit.gihubsample.models.SearchRepoPojo;

public interface HomeViewInterface {

    void showToast(String s);
    void showProgressBar();
    void hideProgressBar();
    void displayRepo(SearchRepoPojo searchRepoPojo);
    void displayError(String s);

}
