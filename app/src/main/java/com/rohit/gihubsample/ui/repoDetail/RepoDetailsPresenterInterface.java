package com.rohit.gihubsample.ui.repoDetail;

public interface RepoDetailsPresenterInterface {

    void getContributor(String login,String name);
}
