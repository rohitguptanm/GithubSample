package com.rohit.gihubsample.ui.contributorRepo;

import com.rohit.gihubsample.models.ItemsItem;

import java.util.List;

public interface ContributorRepoViewInterface {

    void showToast(String s);
    void showProgressBar();
    void hideProgressBar();
    void displayRepo(List<ItemsItem> contributorPojoList);
    void displayError(String s);

}
