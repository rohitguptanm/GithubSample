package com.rohit.gihubsample.ui.contributorRepo;

public interface ContributorRepoPresenterInterface {

    void getAllRepo(String login);
}
