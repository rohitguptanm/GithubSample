package com.rohit.gihubsample.ui.home;

public interface HomePresenterInterface {

    void getRepo(String query,String order,boolean isProgressbar);
}
