package com.rohit.gihubsample.ui.repoDetail;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rohit.gihubsample.R;
import com.rohit.gihubsample.adapters.ContributorAdapter;
import com.rohit.gihubsample.models.ContributorPojo;
import com.rohit.gihubsample.models.SearchRepoPojo;
import com.rohit.gihubsample.ui.WebViewActivity;
import com.rohit.gihubsample.utils.AppConstants;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import java.lang.invoke.ConstantCallSite;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoDetailsActivity extends AppCompatActivity implements RepoDetailsViewInterface {


    RepoDetailsPresenter mRepoDetailsPresenter;
    ContributorAdapter mContributorAdapter;
    List<ContributorPojo> mContributerList = new ArrayList<>();
    public final String TAG = RepoDetailsActivity.class.getSimpleName();
    @BindView(R.id.iv_avtar)
    ImageView mIvAvtar;
    @BindView(R.id.tv_name)
    TextView mTvName;
    @BindView(R.id.rv_contributors)
    RecyclerView mRvContributors;

    String name;
    String login;
    String avtar_img;
    String project_desc;
    String project_link;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_project_link)
    TextView mTvProjectLink;
    @BindView(R.id.tv_project_desc)
    TextView mTvProjectDesc;

    @BindView(R.id.progressbar)
    DilatingDotsProgressBar mDilatingDotsProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        ButterKnife.bind(this);

        name = getIntent().getStringExtra(AppConstants.NAME);
        login = getIntent().getStringExtra(AppConstants.LOGIN);
        avtar_img = getIntent().getStringExtra(AppConstants.AVTAR_IMG);
        project_desc = getIntent().getStringExtra(AppConstants.PROJECT_DESC);
        project_link = getIntent().getStringExtra(AppConstants.PROJECT_LINK);


        Glide.with(this).load(avtar_img).into(mIvAvtar);
        mTvName.setText(name);
        mTvProjectLink.setText(project_link);
        mTvProjectLink.setPaintFlags(mTvProjectLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvProjectDesc.setText(project_desc);

        mTvProjectLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RepoDetailsActivity.this, WebViewActivity.class);
                intent.putExtra(AppConstants.PROJECT_LINK,project_link);
                startActivity(intent);
            }
        });


        mRepoDetailsPresenter = new RepoDetailsPresenter(this);
        mRepoDetailsPresenter.getContributor(login,name);

        mContributorAdapter = new ContributorAdapter(this, mContributerList);
        mRvContributors.setLayoutManager(new GridLayoutManager(this,3));
        mRvContributors.setAdapter(mContributorAdapter);
    }


    @Override
    public void showProgressBar() {
        mDilatingDotsProgressBar.showNow();
    }

    @Override
    public void hideProgressBar() {
        mDilatingDotsProgressBar.hideNow();
    }

    @Override
    public void contributorList(List<ContributorPojo> contributorPojoList) {
        if (contributorPojoList != null && contributorPojoList.size()>0) {
            Log.d(TAG, contributorPojoList.size()+"");
            mContributerList.addAll(contributorPojoList);
            mContributorAdapter.notifyDataSetChanged();
        } else {
            Log.d(TAG, "no contributor: ");
        }
    }


    @Override
    public void showMsg(String s) {

    }
}
