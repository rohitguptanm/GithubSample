package com.rohit.gihubsample.utils;

public class AppConstants {

//    public static final String BASE_URL = "https://api.github.com/search/repositories?q=ro&order=desc&per_page=10&page=1";
    public static final String BASE_URL = "https://api.github.com/";
    public static final String NAME = "NAME";
    public static final String LOGIN = "LOGIN";
    public static final String AVTAR_IMG = "AVTAR_IMG";
    public static final String PROJECT_DESC = "PROJECT_DESC";
    public static final String PROJECT_LINK = "PROJECT_LINK";
}
